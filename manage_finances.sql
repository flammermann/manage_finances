-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: manage_finances
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Account`
--

DROP TABLE IF EXISTS `Account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL,
  `type` enum('cash','bank','fictional','helper') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Account`
--

LOCK TABLES `Account` WRITE;
/*!40000 ALTER TABLE `Account` DISABLE KEYS */;
INSERT INTO `Account` VALUES (1,'Sichteinlagen','bank');
/*!40000 ALTER TABLE `Account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Amount`
--

DROP TABLE IF EXISTS `Amount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Amount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sign` enum('pos','neg') NOT NULL,
  `amount` decimal(14,10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Amount`
--

LOCK TABLES `Amount` WRITE;
/*!40000 ALTER TABLE `Amount` DISABLE KEYS */;
INSERT INTO `Amount` VALUES (1,'neg',1.0000000000),(2,'pos',1.0000000000);
/*!40000 ALTER TABLE `Amount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Attribute`
--

DROP TABLE IF EXISTS `Attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Attribute`
--

LOCK TABLES `Attribute` WRITE;
/*!40000 ALTER TABLE `Attribute` DISABLE KEYS */;
INSERT INTO `Attribute` VALUES (1,'Beverage'),(2,'Hot Beverage'),(3,'Cold Beverage'),(4,'Deposit');
/*!40000 ALTER TABLE `Attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'Foodstuffs');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Friend`
--

DROP TABLE IF EXISTS `Friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Friend`
--

LOCK TABLES `Friend` WRITE;
/*!40000 ALTER TABLE `Friend` DISABLE KEYS */;
INSERT INTO `Friend` VALUES (1,'Felix Lammermann','finances@flammermann.de');
/*!40000 ALTER TABLE `Friend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Location`
--

DROP TABLE IF EXISTS `Location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `establishment` text NOT NULL,
  `street` text DEFAULT NULL,
  `city` text DEFAULT NULL,
  `state` text DEFAULT NULL,
  `country` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Location`
--

LOCK TABLES `Location` WRITE;
/*!40000 ALTER TABLE `Location` DISABLE KEYS */;
INSERT INTO `Location` VALUES (1,'Mr. Bleck','Untere Karlstraße 1','Erlangen','Bavaria','Germany');
/*!40000 ALTER TABLE `Location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(14,2) NOT NULL,
  `title` text NOT NULL,
  `size` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (1,3.90,'Chai Latte','Large',NULL),(2,2.80,'Dr. Pepper Real Sugar','0.33L Dose',NULL),(3,0.25,'Pfand','PET-Flasche',NULL),(4,3.30,'Chai Latte','Medium',NULL);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductAmount`
--

DROP TABLE IF EXISTS `ProductAmount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductAmount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `product_id` (`product_id`),
  KEY `amount_id` (`amount_id`),
  CONSTRAINT `ProductAmount_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `ProductAmount_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `Product` (`id`),
  CONSTRAINT `ProductAmount_ibfk_3` FOREIGN KEY (`amount_id`) REFERENCES `Amount` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductAmount`
--

LOCK TABLES `ProductAmount` WRITE;
/*!40000 ALTER TABLE `ProductAmount` DISABLE KEYS */;
INSERT INTO `ProductAmount` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,2,4,1),(5,2,3,2);
/*!40000 ALTER TABLE `ProductAmount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductAttribute`
--

DROP TABLE IF EXISTS `ProductAttribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductAttribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `attribute_id` (`attribute_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `ProductAttribute_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `ProductAttribute_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `Attribute` (`id`),
  CONSTRAINT `ProductAttribute_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `Product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductAttribute`
--

LOCK TABLES `ProductAttribute` WRITE;
/*!40000 ALTER TABLE `ProductAttribute` DISABLE KEYS */;
INSERT INTO `ProductAttribute` VALUES (1,1,1,1),(2,1,1,2),(3,1,2,1),(4,1,2,3),(5,1,3,4),(6,2,4,1),(7,2,4,2),(8,2,3,4);
/*!40000 ALTER TABLE `ProductAttribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subcategory`
--

DROP TABLE IF EXISTS `Subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategory` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subcategory`
--

LOCK TABLES `Subcategory` WRITE;
/*!40000 ALTER TABLE `Subcategory` DISABLE KEYS */;
INSERT INTO `Subcategory` VALUES (1,'Restaurant & Café');
/*!40000 ALTER TABLE `Subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SubcategoryCategory`
--

DROP TABLE IF EXISTS `SubcategoryCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SubcategoryCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategory_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategory_id` (`subcategory_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `SubcategoryCategory_ibfk_1` FOREIGN KEY (`subcategory_id`) REFERENCES `Subcategory` (`id`),
  CONSTRAINT `SubcategoryCategory_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SubcategoryCategory`
--

LOCK TABLES `SubcategoryCategory` WRITE;
/*!40000 ALTER TABLE `SubcategoryCategory` DISABLE KEYS */;
INSERT INTO `SubcategoryCategory` VALUES (1,1,1);
/*!40000 ALTER TABLE `SubcategoryCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Transaction`
--

DROP TABLE IF EXISTS `Transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `occasion` text DEFAULT NULL,
  `advanced` enum('pos','neg') DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `receipt` text DEFAULT NULL,
  `note` text DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Transaction`
--

LOCK TABLES `Transaction` WRITE;
/*!40000 ALTER TABLE `Transaction` DISABLE KEYS */;
INSERT INTO `Transaction` VALUES (1,NULL,NULL,'2019-08-18 12:22:00',NULL,'testing','2019-08-20 15:44:31'),(2,NULL,NULL,'2019-08-18 13:58:00',NULL,'more testing','2019-08-20 15:45:14');
/*!40000 ALTER TABLE `Transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionAccount`
--

DROP TABLE IF EXISTS `TransactionAccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionAccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `TransactionAccount_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionAccount_ibfk_2` FOREIGN KEY (`account_id`) REFERENCES `Account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionAccount`
--

LOCK TABLES `TransactionAccount` WRITE;
/*!40000 ALTER TABLE `TransactionAccount` DISABLE KEYS */;
INSERT INTO `TransactionAccount` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `TransactionAccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionAdvancedRefunded`
--

DROP TABLE IF EXISTS `TransactionAdvancedRefunded`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionAdvancedRefunded` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `advanced_id` int(11) NOT NULL,
  `refunded_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `advanced_id` (`advanced_id`),
  KEY `refunded_id` (`refunded_id`),
  CONSTRAINT `TransactionAdvancedRefunded_ibfk_1` FOREIGN KEY (`advanced_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionAdvancedRefunded_ibfk_2` FOREIGN KEY (`refunded_id`) REFERENCES `Transaction` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionAdvancedRefunded`
--

LOCK TABLES `TransactionAdvancedRefunded` WRITE;
/*!40000 ALTER TABLE `TransactionAdvancedRefunded` DISABLE KEYS */;
/*!40000 ALTER TABLE `TransactionAdvancedRefunded` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionCategory`
--

DROP TABLE IF EXISTS `TransactionCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `TransactionCategory_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionCategory_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionCategory`
--

LOCK TABLES `TransactionCategory` WRITE;
/*!40000 ALTER TABLE `TransactionCategory` DISABLE KEYS */;
INSERT INTO `TransactionCategory` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `TransactionCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionFriend`
--

DROP TABLE IF EXISTS `TransactionFriend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionFriend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `friend_id` (`friend_id`),
  CONSTRAINT `TransactionFriend_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionFriend_ibfk_2` FOREIGN KEY (`friend_id`) REFERENCES `Friend` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionFriend`
--

LOCK TABLES `TransactionFriend` WRITE;
/*!40000 ALTER TABLE `TransactionFriend` DISABLE KEYS */;
INSERT INTO `TransactionFriend` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `TransactionFriend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionLocation`
--

DROP TABLE IF EXISTS `TransactionLocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionLocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `TransactionLocation_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionLocation_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `Location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionLocation`
--

LOCK TABLES `TransactionLocation` WRITE;
/*!40000 ALTER TABLE `TransactionLocation` DISABLE KEYS */;
INSERT INTO `TransactionLocation` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `TransactionLocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionProduct`
--

DROP TABLE IF EXISTS `TransactionProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `TransactionProduct_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionProduct_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `Product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionProduct`
--

LOCK TABLES `TransactionProduct` WRITE;
/*!40000 ALTER TABLE `TransactionProduct` DISABLE KEYS */;
INSERT INTO `TransactionProduct` VALUES (1,1,1),(2,1,2),(3,1,3),(4,2,4),(5,2,3);
/*!40000 ALTER TABLE `TransactionProduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransactionSubcategory`
--

DROP TABLE IF EXISTS `TransactionSubcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionSubcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `subcategory_id` (`subcategory_id`),
  CONSTRAINT `TransactionSubcategory_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `Transaction` (`id`),
  CONSTRAINT `TransactionSubcategory_ibfk_2` FOREIGN KEY (`subcategory_id`) REFERENCES `Subcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransactionSubcategory`
--

LOCK TABLES `TransactionSubcategory` WRITE;
/*!40000 ALTER TABLE `TransactionSubcategory` DISABLE KEYS */;
INSERT INTO `TransactionSubcategory` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `TransactionSubcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'manage_finances'
--
/*!50003 DROP PROCEDURE IF EXISTS `purchase` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `purchase`( in this_account varchar(50), in this_type enum('cash','bank','fictional','helper'), in this_category text, in this_subcategory text, in this_occasion text, in this_datetime datetime, in this_establishment text, in this_street text, in this_city text, in this_state text, in this_country text, in this_advanced enum('pos','neg'), in this_name varchar(70), in this_email varchar(255), in this_receipt text, in this_note text )
BEGIN insert into Transaction (datetime) values (this_datetime); set @transaction_id = (select max(id) from Transaction); if (this_occasion is not null) then update Transaction set occasion = this_occasion where id = @transaction_id; end if; if (this_advanced is not null) then update Transaction set advanced = this_advanced where id = @transaction_id; end if; if (this_receipt is not null) then update Transaction set receipt = this_receipt where id = @transaction_id; end if; if (this_note is not null) then update Transaction set note = this_note where id = @transaction_id; end if; if not (select exists (select 1 from Account where this_account = account)) then insert into Account (account) values (this_account); end if; set @account_id = (select id from Account where this_account = account); update Account set type = this_type where id = @account_id; insert into TransactionAccount (transaction_id, account_id) values (@transaction_id, @account_id); if not (select exists (select 1 from Category where this_category = category)) then insert into Category (category) values (this_category); end if; set @category_id = (select id from Category where this_category = category); if not (select exists (select 1 from Subcategory where this_subcategory = subcategory)) then insert into Subcategory (subcategory) values (this_subcategory); end if; set @subcategory_id = (select id from Subcategory where this_subcategory = subcategory); if not (select exists (select 1 from SubcategoryCategory where @subcategory_id = subcategory_id and @category_id = category_id)) then insert into SubcategoryCategory (subcategory_id, category_id) values (@subcategory_id, @category_id); end if; insert into TransactionCategory (transaction_id, category_id) values (@transaction_id, @category_id); insert into TransactionSubcategory (transaction_id, subcategory_id) values (@transaction_id, @subcategory_id); if not (select exists (select 1 from Location where this_establishment = establishment and this_street = street and this_city = city)) then insert into Location (establishment) values (this_establishment); set @location_id = (select max(id) from Location); else set @location_id = (select id from Location where this_establishment = establishment and this_street = street and this_city = city); end if; if (this_street is not null) then update Location set street = this_street where id = @location_id; end if; if (this_city is not null) then update Location set city = this_city where id = @location_id; end if; if (this_state is not null) then update Location set state = this_state where id = @location_id; end if; if (this_country is not null) then update Location set country = this_country where id = @location_id; end if; insert into TransactionLocation (transaction_id, location_id) values (@transaction_id, @location_id); if not (select exists (select 1 from Friend where this_name = name)) then insert into Friend (name) values (this_name); end if; set @friend_id = (select id from Friend where this_name = name); if (this_email is not null) then update Friend set email = this_email where id = @friend_id; end if; insert into TransactionFriend (transaction_id, friend_id) values (@transaction_id, @friend_id); END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `purchaseProduct` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `purchaseProduct`( in this_amount text, in this_title text, in this_size text, in this_sign enum('pos','neg'), in this_value decimal(14,2), in this_attribute_one text, in this_attribute_two text, in this_attribute_three text, in this_attribute_four text, in this_attribute_five text, in this_image text )
BEGIN set @transaction_id = (select max(id) from Transaction); if not (select exists (select 1 from Product where this_value = value and this_title = title and this_size = size)) then insert into Product (value, title) values (this_value, this_title); set @product_id = (select max(id) from Product); else set @product_id = (select id from Product where this_value = value and this_title = title and this_size = size); end if; if (this_size is not null) then update Product set size = this_size where id = @product_id; end if; if (this_image is not null) then update Product set image = this_image where id = @product_id; end if; insert into TransactionProduct (transaction_id, product_id) values (@transaction_id, @product_id); if not (select exists (select 1 from Amount where this_sign = sign and this_amount = amount)) then insert into Amount (sign, amount) values (this_sign, this_amount); end if; set @amount_id = (select id from Amount where this_sign = sign and this_amount = amount); insert into ProductAmount (transaction_id, product_id, amount_id) values (@transaction_id, @product_id, @amount_id); if (this_attribute_one is not null) then if not (select exists (select 1 from Attribute where this_attribute_one = attribute)) then insert into Attribute (attribute) values (this_attribute_one); end if; set @attribute_one_id = (select id from Attribute where this_attribute_one = attribute); insert into ProductAttribute (transaction_id, product_id, attribute_id) values (@transaction_id, @product_id, @attribute_one_id); end if; if (this_attribute_two is not null) then if not (select exists (select 1 from Attribute where this_attribute_two = attribute)) then insert into Attribute (attribute) values (this_attribute_two); end if; set @attribute_two_id = (select id from Attribute where this_attribute_two = attribute); insert into ProductAttribute (transaction_id, product_id, attribute_id) values (@transaction_id, @product_id, @attribute_two_id); end if; if (this_attribute_three is not null) then if not (select exists (select 1 from Attribute where this_attribute_three = attribute)) then insert into Attribute (attribute) values (this_attribute_three); end if; set @attribute_three_id = (select id from Attribute where this_attribute_three = attribute); insert into ProductAttribute (transaction_id, product_id, attribute_id) values (@transaction_id, @product_id, @attribute_three_id); end if; if (this_attribute_four is not null) then if not (select exists (select 1 from Attribute where this_attribute_four = attribute)) then insert into Attribute (attribute) values (this_attribute_four); end if; set @attribute_four_id = (select id from Attribute where this_attribute_four = attribute); insert into ProductAttribute (transaction_id, product_id, attribute_id) values (@transaction_id, @product_id, @attribute_four_id); end if; if (this_attribute_five is not null) then if not (select exists (select 1 from Attribute where this_attribute_five = attribute)) then insert into Attribute (attribute) values (this_attribute_five); end if; set @attribute_five_id = (select id from Attribute where this_attribute_five = attribute); insert into ProductAttribute (transaction_id, product_id, attribute_id) values (@transaction_id, @product_id, @attribute_five_id); end if; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `selectTransaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `selectTransaction`()
BEGIN drop table if exists allTransaction; create temporary table allTransaction select Transaction.id, account, type, category, subcategory, occasion, amount, title, size, sign, value, group_concat(attribute order by attribute separator ', ') as attribute, image, datetime, establishment, street, city, state, country, advanced, name, email, receipt, note from Transaction join TransactionAccount on Transaction.id = TransactionAccount.transaction_id left join Account on account_id = Account.id left join TransactionLocation on Transaction.id = TransactionLocation.transaction_id left join Location on location_id = Location.id left join TransactionCategory on Transaction.id = TransactionCategory.transaction_id left join Category on category_id = Category.id left join TransactionSubcategory on Transaction.id = TransactionSubcategory.transaction_id left join Subcategory on subcategory_id = Subcategory.id left join TransactionFriend on Transaction.id = TransactionFriend.transaction_id left join Friend on friend_id = Friend.id left join TransactionProduct on Transaction.id = TransactionProduct.transaction_id left join Product on product_id = Product.id left join ProductAmount on Transaction.id = ProductAmount.transaction_id and Product.id = ProductAmount.product_id left join Amount on amount_id = Amount.id left join ProductAttribute on Transaction.id = ProductAttribute.transaction_id and Product.id = ProductAttribute.product_id left join Attribute on attribute_id = Attribute.id group by Transaction.id, Product.id order by datetime, title; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `selectTransactionByDatetime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `selectTransactionByDatetime`( in this_transaction_datetime datetime )
BEGIN drop table if exists allTransaction; call selectTransaction; select * from allTransaction where datetime >= this_transaction_datetime and datetime < (select date_add(this_transaction_datetime, interval 1 minute)); END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `selectTransactionByDatetimeRange` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `selectTransactionByDatetimeRange`( in this_transaction_datetime_start datetime, in this_transaction_datetime_end datetime )
BEGIN drop table if exists allTransaction; call selectTransaction; select * from allTransaction where datetime >= this_transaction_datetime_start and datetime < (select date_add(this_transaction_datetime_end, interval 1 minute)); END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `selectTransactionById` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `selectTransactionById`( in this_transaction_id int )
BEGIN drop table if exists allTransaction; call selectTransaction; select * from allTransaction where id = this_transaction_id; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `showAllTableContents` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `showAllTableContents`()
BEGIN show tables; select * from Transaction; select * from Account; select * from TransactionAccount; select * from Category; select * from TransactionCategory; select * from Subcategory; select * from TransactionSubcategory; select * from SubcategoryCategory; select * from Location; select * from TransactionLocation; select * from Friend; select * from TransactionFriend; select * from Product; select * from TransactionProduct; select * from Amount; select * from ProductAmount; select * from Attribute; select * from ProductAttribute; select * from TransactionAdvancedRefunded; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `showAllTables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `showAllTables`()
BEGIN show tables; describe Transaction; describe Account; describe TransactionAccount; describe Category; describe TransactionCategory; describe Subcategory; describe TransactionSubcategory; describe SubcategoryCategory; describe Location; describe TransactionLocation; describe Friend; describe TransactionFriend; describe Product; describe TransactionProduct; describe Amount; describe ProductAmount; describe Attribute; describe ProductAttribute; describe TransactionAdvancedRefunded; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `showTransaction` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `showTransaction`( in this_transaction_id int )
BEGIN select Transaction.id, account, type, category, subcategory, occasion, amount, title, size, sign, value, group_concat(attribute order by attribute separator ', ') as attribute, image, datetime, establishment, street, city, state, country, advanced, name, email, receipt, note from Transaction join TransactionAccount on Transaction.id = TransactionAccount.transaction_id left join Account on account_id = Account.id left join TransactionLocation on Transaction.id = TransactionLocation.transaction_id left join Location on location_id = Location.id left join TransactionCategory on Transaction.id = TransactionCategory.transaction_id left join Category on category_id = Category.id left join TransactionSubcategory on Transaction.id = TransactionSubcategory.transaction_id left join Subcategory on subcategory_id = Subcategory.id left join TransactionFriend on Transaction.id = TransactionFriend.transaction_id left join Friend on friend_id = Friend.id left join TransactionProduct on Transaction.id = TransactionProduct.transaction_id left join Product on product_id = Product.id left join ProductAmount on Transaction.id = ProductAmount.transaction_id and Product.id = ProductAmount.product_id left join Amount on amount_id = Amount.id left join ProductAttribute on Transaction.id = ProductAttribute.transaction_id and Product.id = ProductAttribute.product_id left join Attribute on attribute_id = Attribute.id where Transaction.id = this_transaction_id group by Transaction.id, Product.id order by datetime, title; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `truncateAllTables` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`groot`@`localhost` PROCEDURE `truncateAllTables`()
BEGIN show tables; truncate Transaction; truncate Account; truncate TransactionAccount; truncate Category; truncate TransactionCategory; truncate Subcategory; truncate TransactionSubcategory; truncate SubcategoryCategory; truncate Location; truncate TransactionLocation; truncate Friend; truncate TransactionFriend; truncate Product; truncate TransactionProduct; truncate Amount; truncate ProductAmount; truncate Attribute; truncate ProductAttribute; truncate TransactionAdvancedRefunded; END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-07 23:53:50
