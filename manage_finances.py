from flask import Flask, render_template, json, request
from flaskext.mysql import MySQL
import re


app = Flask(__name__)

app.config['MYSQL_DATABASE_USER'] = 'groot'
app.config['MYSQL_DATABASE_PASSWORD'] = 'groot'
app.config['MYSQL_DATABASE_DB'] = 'manage_finances'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

mysql = MySQL()
mysql.init_app(app)

conn = mysql.connect()
cursor = conn.cursor()


def getAll(this_dict, this_key):
    return [ value for key, value in this_dict.items() if key.startswith(this_key) ]


@app.route("/")
def main():
    
    return render_template('index.html')


@app.route('/add_purchase')
def add_purchase():

    return render_template('add_purchase.html')


@app.route('/add_purchase_to_db', methods = ['POST'])
def add_purchase_to_db():

    formData = request.form

    _account = formData['account']
    _type = formData['type']
    _category = formData['category']
    _subcategory = formData['subcategory']
    _occasion = formData['occasion']
    
    _datetime = formData['date'] + " " + formData['time']
    _establishment = formData['establishment']
    _street = formData['street']
    _city = formData['city']
    _state = formData['state']
    _country = formData['country']
    
    _receipt = formData['receipt']
    _note = formData['note']
    
    if _subcategory == "":
        _subcategory = _category

    if _occasion == "":
        _occasion = None

    if _street == "":
        _street = None

    if _city == "":
        _city = None

    if _state == "":
        _state = None

    if _country == "":
        _country = None

    if _receipt == "":
        _receipt = None

    if _note == "":
        _note = None

    _name = getAll(formData, 'name')
    _email = getAll(formData, 'email')
    _paid = getAll(formData, 'paid')
    _percentage_paid = getAll(formData, 'percentage-paid')
    _used = getAll(formData, 'used')
    _percentage_used = getAll(formData, 'percentage-used')
    
    _amount = getAll(formData, 'amount')
    _title = getAll(formData, 'title')
    _size = getAll(formData, 'size')
    _sign = getAll(formData, 'sign')
    _value = getAll(formData, 'value')
    _attribute = [re.split(r"\s*,\s*", attrList) for attrList in getAll(formData, 'attribute')]
    _image = getAll(formData, 'image')
    
    for i in range(len(_name)):

        _this_name = _name[i]
        _this_email = _email[i]
        _this_paid = _paid[i]
        _this_percentage_paid = _percentage_paid[i]
        _this_used = _used[i]
        _this_percentage_used = _percentage_used[i]
        
        if _this_email == "":
            _this_email = None

        #if len(_name) == 1:
        #    _this_advanced = None
        #elif :
        #    _this_advanced = "pos"
        #elif ():
        #    _this_advanced = "neg"
        
        _this_advanced = None

        cursor.callproc('purchase',(_account, _type, _category, _subcategory,
            _occasion, _datetime, _establishment, _street, _city, _state,
            _country, _this_advanced, _this_name, _this_email, _receipt,
            _note))

        for j in range(len(_title)):

            _this_amount = float(_amount[j]) * float(_percentage_used[0]) / 100
            _this_title = _title[j]
            _this_size = _size[j]
            _this_sign = _sign[j]
            _this_value = float(_value[j]) * float(_percentage_used[0]) / 100
            _this_attribute = _attribute[j][:5] + [None]*(5 - len(_attribute[j]))
            _this_image = _image[j]

            if _this_size == "":
                _this_size = None

            if _this_image == "":
                _this_image = None

            cursor.callproc('purchaseProduct',(_this_amount, _this_title,
                _this_size, _this_sign, _this_value, _this_attribute[0],
                _this_attribute[1], _this_attribute[2], _this_attribute[3],
                _this_attribute[4], _this_image))

    queryFeedback = cursor.fetchall()

    if len(queryFeedback) == 0:
        
        conn.commit()

        return json.dumps({"account": _account, "type": _type, "category":
            _category, "subcategory": _subcategory, "occasion": _occasion,
            "amount": _amount, "title": _title, "size": _size, "sign": _sign,
            "value": _value, "attribute": _attribute, "image": _image,
            "datetime": _datetime, "establishment":  _establishment, "street":
            _street, "city": _city, "state": _state, "country": _country,
            "name": _name, "email": _email, "paid": _paid, "percentage paid":
            _percentage_paid, "used": _used, "percentage used":
            _percentage_used, "receipt": _receipt, "note": _note})
    
    else:
        
        return json.dumps({'html':'<span>something went wrong</span>'})


if __name__ == "__main__":
    app.run(debug = True)
