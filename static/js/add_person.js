$(document).ready(function () {

  function pad(num, size) {
      var s = num+"";
      while (s.length < size) s = "0" + s;
      return s;
  }
  
  var counter_person = 0;

  $("#addPerson").on("click", function () {
  
    var newRow = $("<tr>");
    var cols = "";

    cols += '<tr class = "d-flex">';
    cols += '  <td class = "col">';
    cols += '    <input class = "form-control" type = "name" name = "name' + pad(counter_person + 1, 3) + '" id = "name' + pad(counter_person + 1, 3) + '" placeholder = "Name">';
    cols += '  </td>';
    cols += '  <td class = "col">';
    cols += '    <input class = "form-control" type = "email" name = "email' + pad(counter_person + 1, 3) + '" id = "email' + pad(counter_person + 1, 3) + '" placeholder = "Email Address">';
    cols += '  </td>';
    cols += '  <td align = "center">';
    cols += '    <div class = "btn-group-toggle" data-toggle = "buttons">';
    cols += '      <label class = "btn btn-outline-primary">';
    cols += '        <input type = "checkbox" name = "paid' + pad(counter_person + 1, 3) + '" id = "paid' + pad(counter_person + 1, 3) + 'true"  value = "true" autocomplete = "off">';
    cols += '        <input type = "hidden"   name = "paid' + pad(counter_person + 1, 3) + '" id = "paid' + pad(counter_person + 1, 3) + 'false" value = "false" />';
    cols += '        Paid';
    cols += '      </label>';
    cols += '    </div>';
    cols += '  </td>';
    cols += '  <td>';
    cols += '    <div class = "input-group">';
    cols += '      <input class = "form-control text-right" type = "number" name = "percentage-paid' + pad(counter_person + 1, 3) + '" id = "percentage-paid' + pad(counter_person + 1, 3) + '" min = "0" max = "100" value = "0" style = "direction: rtl; width: 5rem">';
    cols += '      <div class = "input-group-append">';
    cols += '        <span class = "input-group-text" id = "percentage-unit">%</span>';
    cols += '      </div>';
    cols += '    </div>';
    cols += '  </td>';
    cols += '  <td align = "center">';
    cols += '    <div class = "btn-group-toggle" data-toggle = "buttons">';
    cols += '      <label class = "btn btn-outline-primary active">';
    cols += '        <input type = "checkbox" name = "used' + pad(counter_person + 1, 3) + '" id = "used' + pad(counter_person + 1, 3) + 'true"  value = "true" autocomplete = "off" checked>';
    cols += '        <input type = "hidden"   name = "used' + pad(counter_person + 1, 3) + '" id = "used' + pad(counter_person + 1, 3) + 'false" value = "false" />';
    cols += '        Used';
    cols += '      </label>';
    cols += '    </div>';
    cols += '  </td>';
    cols += '  <td>';
    cols += '    <div class = "input-group">';
    cols += '      <input class = "form-control text-right" type = "number" name = "percentage-used' + pad(counter_person + 1, 3) + '" id = "percentage-used' + pad(counter_person + 1, 3) + '" min = "0" max = "100" value = "0" style = "direction: rtl; width: 5rem">';
    cols += '      <div class = "input-group-append">';
    cols += '        <span class = "input-group-text" id = "percentage-unit">%</span>';
    cols += '      </div>';
    cols += '    </div>';
    cols += '  </td>';
    cols += '  <td align = "center">';
    cols += '    <input type = "button" class = "btn btn-outline-danger deleteRow" value = "Delete"></td>';
    cols += '  </td>';
    cols += '</tr>';

    newRow.append(cols);
    $("table.personList").append(newRow);
    
    counter_person++;
  
    console.log(counter_person);

  });

  $("table.personList").on("click", ".deleteRow", function (event) {
  
    $(this).closest("tr").remove();       
    counter_person -= 1
  
    console.log(counter_person);

  });

});

