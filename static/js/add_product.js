$(document).ready(function () {

  function pad(num, size) {
      var s = num+"";
      while (s.length < size) s = "0" + s;
      return s;
  }
  
  var counter_product = 0;

  $("#addProduct").on("click", function () {
  
    var newRow = $("<tr>");
    var cols = "";

    cols += '<tr class = "d-flex">';
    cols += '  <td>';
    cols += '    <input class = "form-control text-right" type = "number" name = "amount' + pad(counter_product + 1, 4) + '" id = "amount' + pad(counter_product + 1, 4) + '" min = "0" value = "1" style = "direction: rtl; width: 4rem" required>';
    cols += '  </td>';
    cols += '  <td class = "col">';
    cols += '    <input class = "form-control" type = "text" name = "title' + pad(counter_product + 1, 4) + '" id = "title' + pad(counter_product + 1, 4) + '" placeholder = "Title" required>';
    cols += '  </td>';
    cols += '  <td class = "col-2">';
    cols += '    <input class = "form-control" type = "text" name = "size' + pad(counter_product + 1, 4) + '" id = "size' + pad(counter_product + 1, 4) + '" placeholder = "Size">';
    cols += '  </td>';
    cols += '  <td align = "center">';
    cols += '    <div class = "btn-group btn-group-toggle" data-toggle = "buttons">';
    cols += '      <label class = "btn btn-outline-danger active" style = "width: 2.5rem; font-weight: bold">';
    cols += '        <input type = "radio" name = "sign' + pad(counter_product + 1, 4) + '" id = "sign' + pad(counter_product + 1, 4) + 'neg" value = "neg" autocomplete = "off" checked> -';
    cols += '      </label>';
    cols += '      <label class = "btn btn-outline-primary" style = "width: 2.5rem; font-weight: bold">';
    cols += '        <input type = "radio" name = "sign' + pad(counter_product + 1, 4) + '" id = "sign' + pad(counter_product + 1, 4) + 'pos" value = "pos" autocomplete = "off"> +';
    cols += '      </label>';
    cols += '    </div>';
    cols += '  </td>';
    cols += '  <td>';
    cols += '    <div class = "input-group">';
    cols += '      <input class = "form-control text-right" type = "number" name = "value' + pad(counter_product + 1, 4) + '" id = "value' + pad(counter_product + 1, 4) + '" value = "1.00" step = "0.01" min = "0" style = "direction: rtl; width    : 6rem" required>';
    cols += '      <div class = "input-group-append">';
    cols += '        <span class = "input-group-text" id = "value-unit">€</span>';
    cols += '      </div>';
    cols += '    </div>';
    cols += '  </td>';
    cols += '  <td class = "col-2">';
    cols += '    <input class = "form-control" type = "text" name = "attribute' + pad(counter_product + 1, 4) + '" id = "attribute' + pad(counter_product + 1, 4) + '" placeholder = "Attributes">';
    cols += '  </td>';
    cols += '  <td class = "col-2">';
    cols += '    <input type = "file" class = "file">';
    cols += '      <div class = "input-group">';
    cols += '        <input type = "text" class = "form-control" placeholder = "Image" name = "image' + pad(counter_product + 1, 4) + '" id = "image' + pad(counter_product + 1, 4) + '" aria-label = "image' + pad(counter_product + 1, 4) + '" aria-describedby = "basic-addon2">';
    cols += '        <div class = "input-group-append">';
    cols += '          <button class = "btn btn-outline-secondary browse" type = "button">Browse</button>';
    cols += '        </div>';
    cols += '      </div>';
    cols += '    </input>';
    cols += '  </td>';
    cols += '  <td align = "center">';
    cols += '    <input type = "button" class = "btn btn-outline-danger deleteRow" value = "Delete"></td>';
    cols += '  </td>';
    cols += '</tr>';

    newRow.append(cols);
    $("table.productList").append(newRow);
    
    counter_product++;
  
    console.log(counter_product);

  });

  $("table.productList").on("click", ".deleteRow", function (event) {
  
    $(this).closest("tr").remove();       
    counter_product -= 1
  
    console.log(counter_product);

  });

});


// function calculateRow(row) {
// 
//   var price = +row.find('input[name^="price"]').val();
// 
// }
// 
// function calculateGrandTotal() {
//   
//   var grandTotal = 0;
//   
//   $("table.order-list").find('input[name^="price"]').each(function () {
//     
//     grandTotal += +$(this).val();
//   
//   });
//   
//   $("#grandtotal").text(grandTotal.toFixed(2));
// 
// }
