$(document).ready(function () {

  $('#submit').on("click", function () {

    $.ajax({
      
      url: '/add_purchase_to_db',
      data: $('form').serialize(),
      type: 'POST',
      
      success: function(response) {
          console.log(response);
      },
      
      error: function(error) {
          console.log(error);
      }
    
    });
  
  });

});
